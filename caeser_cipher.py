alphabet = "abcdefghijklmnopqrstuvwxyz"
partial_one = ""
partial_two = ""
new_alphabet = ""
message_input = input("Enter the message you want to cipher: ")
num_input = int(input("Enter the number you want to shift by: "))

if num_input == 0:
    new_alphabet = alphabet
elif num_input > 0:
    partial_one = alphabet[:num_input]
    partial_two = alphabet[num_input:]
    new_alphabet = partial_two + partial_one
else:
    partial_one = alphabet[:(26 + num_input)]
    partial_two = alphabet[(26 + num_input):]
    new_alphabet = partial_two + partial_one

encrypted_message = ""
for message_index in range(0, len(message_input)):
    if message_input[message_index] == " ":
        encrypted_message += " "
    for alphabet_index in range(0, len(new_alphabet)):
        if message_input[message_index] == alphabet[alphabet_index]:
            encrypted_message += new_alphabet[alphabet_index]

print(encrypted_message)